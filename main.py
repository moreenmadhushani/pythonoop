# create class and objects in python
class Parrot:
    # class attribute
    species = "bird"

    # instance attribute
    def __init__(self, name, age):
        self.name = name
        self.age = age


if __name__ == '__main__':
    blu = Parrot("Blu", 10)
    woo = Parrot("Woo",15)

    print("Blu is a {}".format(blu.__class__.species))
    print("{} is {} years old".format(woo.name,woo.age))


