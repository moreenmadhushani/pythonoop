class Parrot:
    def fly(self):
        print("parrot can fly")

    def swim(self):
        print("parrot can't swim")


class Penguin:
    def fly(self):
        print("penguin can't fly")

    def swim(self):
        print("penguin can swim")


# common interface
def flying_test(bird):
    bird.fly()


def swimming_test(bird):
    bird.swim()


# instantiate class objects
blu = Parrot()
peggy = Penguin()

# passing  the object
flying_test(blu)
flying_test(peggy)

swimming_test(blu)
swimming_test(peggy)
